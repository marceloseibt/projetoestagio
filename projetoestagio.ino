#include <Arduino.h>
#include <MFRC522.h>
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <SPI.h>
#include <brzo_i2c.h>
#include "SSD1306Brzo.h"
#include "OLEDDisplayUi.h"
extern "C"{

#include "user_interface.h"

}


//#include "valida.cpp"

#define   CONTRAST_PIN   9
#define   BACKLIGHT_PIN  7
#define   CONTRAST       110


#define RST_PIN D4
#define SS_PIN D8




//////////////////
///// Config Wifi

int frame = 0;

const char* ssid     = "LARM";
const char* password = "UFSC2017larm";
const char* host = "150.162.234.150";  // Exemplo apenas


float sensor1 = 50;
float sensor2 = 80;
float sensor3 = 90;


////////////////////


/////Config TIMER

os_timer_t mTimer;
 
bool       _timeout = false;
 

////////////////

////// CONFIG MYQL

char REQ_SQL[] = "select * from reserva";
IPAddress server_addr(150,162,234,150);   // IP of the MySQL server
char user[] = "root";                     // MySQL user login username
char passwd[] = "";                 // MySQL user login password
 

/////////////////////////////



////// Config interrupcoes e contadores

const byte interruptPin = 13;
volatile byte interruptCounter = 0;
int numberofInterrupts = 0;
int timeoutCounter = 0;
int leu = 0;

long int wifiTry = 0;

enum estados{ONLINE,OFFLINE}; 
enum estados estadoAtual;
 
enum telas{NADA ,BOOT, APROXIME , CONECTANDO,  FALHA , AUTORIZADO , NAO_AUTORIZADO, SHOW_ID, GET, AUTENTICANDO};
enum telas telaAtual = NADA;

int hora = 0;
int minuto = 0;

///////////////////


//// Funcoes

String RFIDcheck(void);
void tCallback(void *tCall);
int WifiGET(String ID);

void tCallback(void *tCall);
void usrInit(void);
void abrePorta(void);
uint SQLquery(uint id);

////Telas usuario
void lcdHello(void);
void lcdConnect(void);
void LcdWork(void);
void lcdOK(void);
void lcdGET(void);
void lcdAuth(void);
////Submenus Persistentes
void drawBarras(void);
void drawStats(void);
void drawAnim(void);
///// BITMAPS MEMORIA ROM (economizar RAM)  
/// tao no include images.h

////////////////// Fim bitmaps 



//// SETUP 


//Classe mfrc
MFRC522 mfrc522(SS_PIN, RST_PIN);

//classes e arquivos globais do displa
SSD1306Brzo display(0x3c, D3, D2);

OLEDDisplayUi ui     ( &display );

// how many frames are there?

// Overlays are statically drawn on top of a frame eg. a clock





void setup() {
    ///abrir seral e mandar nada

    Serial.begin(115200);
    Serial.println();

    ///habilitar interrupcao
    pinMode(interruptPin, INPUT_PULLUP);
    usrInit();

    ///habilitar SPI
    SPI.begin();
    delay(10);
    SPI.setFrequency(4000000);

    //Inicializar OLED
   
    ui.disableAllIndicators();
    // Initialising the UI will init the display too.
    ui.init();

    display.flipScreenVertically();
    //incializar RFID
    mfrc522.PCD_Init();
    ////Inicializar WIFI

    LcdWork();
    frame = 0;
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED || wifiTry > 500) {
        
        // 60 fps 16ms por quadro
        drawAnim();
        delay(5);
        /*
        ///antigo loading
        
        
        display.drawString(0+(10*wifiTry),0,"#");
        display.display();
        Serial.print(".");
        */
        wifiTry++;
    }

    display.clear();
    display.display();
    wifiTry = 0; 
    ////Modo funcionamento offline caso wifi nao esteja disponivel
    if(WiFi.status() != WL_CONNECTED){
        estadoAtual = ONLINE;
        Serial.println("ONLINE");
        drawStats();

    }
    else{
        estadoAtual = OFFLINE;
        Serial.println("OFFLINE");
        drawStats();
    }


    Serial.println(WiFi.localIP());
    void usrInit(void);
    lcdHello();
    drawStats();
}

///// MAIN LOOOOPP @@@@@@@

void loop() {
    
    lcdHello();
    String ID = "nada";
    if (_timeout){

        drawStats();
        ///Passou 1 segundo, fazer algo
        timeoutCounter++;
        leu = 0;
        _timeout = false;
    }
    //// Ta na hora de ler?
    if(leu == 0){
    ID = RFIDcheck();
    leu = 1;
    }
    if(ID != "nada")
    {    
        if(WifiGET(ID) == 1){
        ///TudoCorreto
        abrePorta();
        }
        ID = "nada";
    }


    ///// Passou 5 segundos e nada  aconteceu, o que fazer?
    if(timeoutCounter == 5 ){

        timeoutCounter = 0;
    }
}


String RFIDcheck(void){
    // Procura por cartao RFID
    if ( ! mfrc522.PICC_IsNewCardPresent()) 
    {
      //// CADE O CARTAO? 
    }
    // Seleciona o cartao RFID e le
    if ( ! mfrc522.PICC_ReadCardSerial()) 
    {
        //// bora entao
    }
    //Mostra UID na serial
    Serial.print("UID da tag :");


    String conteudo= "";
    /// percorre matriz de bytes
    for (byte i = 0; i < mfrc522.uid.size; i++) 
    {   /// menor que 10? printa so numeros, maior que dez imprime HEX
       // Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
       // Serial.print(mfrc522.uid.uidByte[i], HEX);
        conteudo.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
        conteudo.concat(String(mfrc522.uid.uidByte[i], HEX));
    }
    if(conteudo.toInt() == 0){
        Serial.println("Sem Tag");
        conteudo = "nada";
    }
    else{
        conteudo.toUpperCase();
        Serial.println(conteudo);
    }
    /// Retorna o conteudo da TAG
    return conteudo;

}
 
 ///////////////////////////////////////////////////////////////////////////////////////////////
int WifiGET(String ID){ 

    WiFiClient client; 
    const int httpPort = 8080;
    int counter = 0;
    if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    /// erro 2 nao conecta ao  servidor, servidor offline
    return 2;
  }
    String url = "/ReservaSala/pesquisaesp.php?sala=101&bloco=A"; 


    // ENVIA A REQUISIÇÃO HTTP AO SERVIDOR 
    client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                "Host: " + host + "\r\n" +
                "Connection: close\r\n\r\n");
    frame = 0;
    lcdGET();
    drawStats();
    while(client.available() == 0 && counter < 2) {
        
        
        if(_timeout){
            counter++;
        }
        
    }
    if(!client.available()){

        //conectado mas nao recebe request
        return 3;
    }
    lcdAuth();
    drawStats();
    /// conectou? show tratar dados agora
    while (client.available()) {
    String aux;
    String IDb = "";
    String line = client.readStringUntil('\r');
    Serial.println("conteudo da linha");
    Serial.println(line);
    hora = (line.substring(0,1)).toInt();
    minuto = (line.substring(3,4).toInt());
    IDb =   line.substring((line.indexOf(";")+1),line.indexOf(";"));

    client.stop();
        if(ID.compareTo(IDb) == 0)
        {
            ///autorizado
            return 1;   
        }
    
    break;
    }
    ///Usuario não encontrado
    return 0;
}




void tCallback(void *tCall){
    _timeout = true;
}
 
void usrInit(void){
    os_timer_setfn(&mTimer, tCallback, NULL);
    os_timer_arm(&mTimer, 1000, true);
}

void abrePorta(void){


}


void LcdWork(void){

    if(telaAtual != CONECTANDO){
        display.setFont(ArialMT_Plain_24);
        display.drawString(0,16,"Conectando");
        display.display();
    telaAtual = CONECTANDO;
    }
}

void lcdHello(void){
    if(telaAtual != BOOT){
        display.clear();
        display.display();
        display.setFont(ArialMT_Plain_24);
        display.drawString(0,16,"Insira ID");
        display.display();
    telaAtual = BOOT;
    }
}



void lcdOK(void){
    if(telaAtual != AUTORIZADO){
        display.clear();
        display.display();
        display.setFont(ArialMT_Plain_24);
        display.drawString(0,16,"Autorizado");
        display.display();
        display.setFont(ArialMT_Plain_24);
        display.drawString(0,36,"Pessoa");
        display.display();
    telaAtual = AUTORIZADO;
    }

}

void lcdGET(void){

    if(telaAtual != GET){
    display.clear();
    display.display();
    display.setFont(ArialMT_Plain_24);
    display.drawString(0,16,"Conectando");
    display.display();
    display.setFont(ArialMT_Plain_24);
    display.drawString(0,36,"Servidor");
    display.display();
    telaAtual = GET ;
    }   

}

void lcdAuth(void){

    if(telaAtual != AUTENTICANDO){
    display.clear();
    display.display();
    display.setFont(ArialMT_Plain_24);
    display.drawString(0,15,"Analise ID");
    display.display();
    telaAtual = AUTENTICANDO ;
    }   

}




void drawAnim(void)
{
    if(frame < 127)
    {
        display.setColor(WHITE);
        display.drawLine(frame,15,frame,0);
        display.display();
        frame++;
    }else if(frame >= 127 && frame < 254){
        display.setColor(BLACK);
        display.drawLine(255 - frame,15, 255 - frame,0);
        display.display();
        frame++;        
    }else if(frame >= 254){
        frame = 0;
    }

    display.setColor(WHITE);

}

void drawStats(void){
    if(WiFi.status() == WL_CONNECTED){
        display.setFont(ArialMT_Plain_10);
        display.drawString(0,50,"online");
        display.display();
        drawBarras();
    }else{
        display.setFont(ArialMT_Plain_10);
        display.drawString(0,53,"offline");
        display.display();  
    }
    if(hora != 0 || minuto != 0){
        display.setFont(ArialMT_Plain_10);
        display.drawString(0,60,String(hora) + ":" + String(minuto));
        display.display();          
    }
}

int potenciaSinal(void){
    
    // 5. High quality: 90% ~= -55db
    // 4. Good quality: 75% ~= -65db
    // 3. Medium quality: 50% ~= -75db
    // 2. Low quality: 30% ~= -85db
    // 1. Unusable quality: 8% ~= -96db
    // 0. No signal
    long rssi = WiFi.RSSI();
    int bars;
    
    if (rssi > -55) { 
        bars = 5;
    } else if (rssi < -55 && rssi > -65) {
        bars = 4;
    } else if (rssi < -65 && rssi > -75) {
        bars = 3;
    } else if (rssi < -75 && rssi > -85) {
        bars = 2;
    } else if (rssi < -85 && rssi > -96) {
        bars = 1;
    } else {
        bars = 0;
    }
    return bars;

}

void drawBarras(void){

    int nivel = potenciaSinal();
    int i = 0;
    display.setColor(BLACK);
    display.fillRect(63,50,64,14);
    display.display();
    display.setColor(WHITE);
    for(i = 0; i < nivel ; i++ ){
        display.fillRect(70 +(i*6),63 -(i*3),4,(i*3));
        display.display();
    }

}
